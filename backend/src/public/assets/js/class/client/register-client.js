'use strict';

(() => {

    loadServers()
    serverButton()

    const form = window.document.getElementById("form")
    const submit = window.document.getElementById("submit")

    submit.addEventListener('click', async (event) => {
        event.preventDefault()

        if (form) {

            //n add verification to all datas

            const data = {

                'name': form[0].value,
                'login': form[1].value,
                'password': form[2].value,
                'email': form[3].value,
                'phone': form[4].value,
                'cellphone': form[5].value,
                'tperson': form[6].value,
                'rg': form[7].value,
                'cpf': form[8].value,
                'birthdate': form[9].value,
                'cep': form[10].value,
                'street': form[11].value,
                'number': form[12].value,
                'description': form[13].value,
                'city_select': form[14].value,
                'uf_select': form[15].value,
                'tsubscriber': form[16].value,
                'server_select': form[17].value,
                'plan_select': form[18].value,
                'free': form[19].value == '1' ? false : true,
                'discount': form[20].value,
                'addition': form[21].value,
                'status': form[22].value == '1' ? false : true,
                'expirationday': form[23].value

            }

            const client = new Client()
            await client.register(data)
                .then(function (response) {
                    // handle success
                    Alert.success(response.data.message)
                    clearInputs()

                })
                .catch(function (error) {
                    // handle error
                    if (error.response.data)
                        Alert.failure(error.response.data.message)

                })

        }


    })

    // select server
    async function loadServers(params) {

        let server = new Server()
        let servers = await server.getAll()

        setTimeout(() => {
            //console.log(servers.data.status)

            if (servers.data.status) {
                let array = servers.data.data
                let options = ``

                for (let index = 0; index < array.length; index++) {
                    const element = array[index];
                    options += `<option value="${element.id}">${element.name}</option>`
                }

                window.document.getElementById('server-select').innerHTML = options

            } else {
                Alert.failure(server.data.message)
            }

        }, 3000)


    }

    function serverButton(params) {
        let opt = window.document.getElementById('server-select')

        console.log(opt)
    
    }

    // select plans


    // get hash form 


    // clear form
    function clearInputs(params) {
        form[0].value = ''
        form[1].value = ''
        form[2].value = ''
        form[3].value = ''
        form[4].value = ''
        form[5].value = ''
        form[7].value = ''
        form[8].value = ''
        form[9].value = ''
        form[10].value = ''
        form[11].value = ''
        form[12].value = ''
        form[13].value = ''
        form[20].value = 0
        form[21].value = 0
        form[23].value = 15
    }

})()


/*
class Index {

    constructor(profile, client) {
        this.profile = profile
        this.client = client
        this.plans = []

        this.init()
    }

    init() {
        this.submit = document.getElementById('submit')

        this.loadOptionsForm()

        this.btnSubmit()
    }

    clearInputForm() {
        this.form = document.getElementById('form')

        this.form[0].value = ''
        this.form[1].value = ''
        this.form[2].value = ''
        this.form[6].value = ''

    }

    btnSubmit() {
        if (this.submit) {
            this.submit.addEventListener('click', async (event) => {
                event.preventDefault()

                this.form = document.getElementById('form')

                let plan = this.plans[this.form[6].value]

                const data = {
                    first_name: this.form[0].value,
                    second_name: this.form[1].value,
                    number: this.form[2].value,
                    plan: plan
                }

                try {
                    const msg = await this.client.save(data)
                    this.clearInputForm()
                    alert(msg)

                } catch (error) {
                    console.log(error)
                    alert(error.message)
                }


            })
        }
    }

    loadOptionsForm() {
        window.addEventListener('load', async () => {

            try {
                const data = await this.profile.getProfiles()

                const select = document.getElementById('profile-select')
                let opts = `<option value="0">Plano</option>`
                this.plans.push('Plano')

                for (let index = 1; index < data.length; index++) {
                    const element = data[index];
                    this.plans.push(element[1].value)

                    let opt = `<option value="${index}">${element[1].value}</option>`
                    opts += opt
                }

                select.innerHTML = opts

            } catch (error) {
                console.log(error)
                alert(error.message)
            }

        })
    }

}

const profile = new Profile()
const client = new Client()
const formsCreateClient = new FCClient(profile, client)




 */