'use strict'

class Alert {
  
    static success(message){
        window.document.getElementById('alerts').innerHTML = `<div class="alert  alert-success alert-dismissible fade show" role="alert">                    
        <span class="badge badge-pill badge-success">Success</span> ${message}.
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>                    
        </div>`
        
        alert(message)

    }

    static failure(message){
        window.document.getElementById('alerts').innerHTML = `<div class="alert  alert-danger alert-dismissible fade show" role="alert">                    
        <span class="badge badge-pill badge-failure">Error</span> ${message}.
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>                    
        </div>`

        alert(message)
    }

}