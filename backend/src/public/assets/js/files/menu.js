const menu = [
    {
        title: "Clientes",
        items: [
            ["Todos", '/v/0'],
            ["Painel", '/v/1'],
            ["Online", '/v/2'],
            ["Alterações de Cadastro", '/v/3'],
            ["Cadastrar", "/pronet/v/register-client.html"]
        ],
        fontawesome: "fa fa-users"
    },
    {
        title: "Integrações",
        items: [
            "Cadastrar Servidor MikRotik",
            "Importar Clientes do MikRotik",
            "Gerencianet"
        ],
        fontawesome: "fa fa-users"
    },
    {
        title: "Financeiro",
        items: [
            "Cobranças"
        ],
        fontawesome: "fa fa-users"
    },
    {
        title: "Ferramentas",
        items: [
            "Mapa"
        ],
        fontawesome: "fa fa-users"
    },
    {
        title: "Sistema",
        items: [
            "Configurações",
            "Usuários",
            "Empresa",
            "Logs",
            "Logs de Conexão",
            "Logs de Integração"
        ],
        fontawesome: "fa fa-users"
    }
]

