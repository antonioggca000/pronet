$.noConflict();

jQuery(document).ready(function ($) {

	"use strict";
	
	[].slice.call(document.querySelectorAll('select.cs-select')).forEach(function (el) {
		new SelectFx(el);
	});

	jQuery('.selectpicker').selectpicker;


	$('#menuToggle').on('click', function (event) {
		$('body').toggleClass('open');
	});

	$('.search-trigger').on('click', function (event) {
		event.preventDefault();
		event.stopPropagation();
		$('.search-trigger').parent('.header-left').addClass('open');
	});

	$('.search-close').on('click', function (event) {
		event.preventDefault();
		event.stopPropagation();
		$('.search-trigger').parent('.header-left').removeClass('open');
	});

	$('title').text('Pronet Admin')

	$('.user-area> a').on('click', function (event) {
		event.preventDefault();
		event.stopPropagation();
		$('.user-menu').parent().removeClass('open');
		$('.user-menu').parent().toggleClass('open');
	});

	(() => {
		const Elmenu = document.getElementById('ul-menu')
		const eventStd = []

		Elmenu.innerHTML = `
		<li class="active">
			<a href="/pronet/dashboard"> <i class="menu-icon fa fa-dashboard"></i>Principal</a>
		</li>

		<li class="menu-item-has-children dropdown">
			<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon ${menu[0].fontawesome}"></i>${menu[0].title}</a>
			
			<ul class="sub-menu children dropdown-menu">
				<li><a class='menu-ref' href="${menu[0].items[0][1]}">${menu[0].items[0][0]}</a></li> 
				<li><a class='menu-ref' href="${menu[0].items[1][1]}">${menu[0].items[1][0]}</a></li> 
				<li><a class='menu-ref' href="${menu[0].items[2][1]}">${menu[0].items[2][0]}</a></li>
				<li><a class='menu-ref' href="${menu[0].items[3][1]}">${menu[0].items[3][0]}</a></li>                         
				<li><a class='menu-ref' href="${menu[0].items[4][1]}">${menu[0].items[4][0]}</a></li>               
			</ul> 
		</li>

	`

		const ElHeader = document.getElementById('header')

		ElHeader.innerHTML = `<div class="header-menu">

			<div class="col-sm-7">
				<a id="menuToggle" class="menutoggle pull-left"><i class="fa fa fa-tasks"></i></a>
				<div class="header-left">				
				
				</div>
			</div>

			<div class="col-sm-5">
				<div class="user-area dropdown float-right">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<img class="user-avatar rounded-circle" src="images/user.png" alt="User Avatar">
					</a>

					<div class="user-menu dropdown-menu">
						<a class="nav-link" href="${user[0].url}"><i class="${user[0].fontawesome}"></i>${user[0].title}</a>
						<a class="nav-link" href="${user[1].url}"><i class="${user[1].fontawesome}"></i>${user[1].title}</a>
						
					</div>
				</div>


			</div>
		</div>`


		/*
		const openFrame = (href) => {
			//#validation
			
			api.get(href)
				.then(r => {
					document.getElementById('frame').innerHTML = r.data
				})
				.catch(console.log)
		}

		const elements = document.querySelectorAll('.menu-ref')
		elements.forEach(element => {
			eventStd.push(element.addEventListener('click', (e) => {
				//console.log(e.target.attributes[2].value) 
				//return e.target.attributes[2].value
				openFrame(e.target.attributes[2].value)
			}))
		});

		*/

	})()




});