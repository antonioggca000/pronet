'use strict'

require('dotenv-safe').config({
    path: process.env.NODE_ENV === 'test' ? '.env.test' : '.env'
})

require('express-async-errors')
const express = require('express')

class App {
    constructor() {
        this.express = express()
        this.routes = require('./routes')

        this.middlewares()
        this.options()
    }

    middlewares() {
        //this.express.use(cors())
        this.express.use(express.json())
        this.express.use('/pronet', this.routes)
        //this.express.use('/pronet/v', express.static('./src/public/'))         
        this.express.use(require('./middlewares/error').handler)
    }

    options() {
        this.express.set('x-powered-by', false)
    }


}

module.exports = new App().express