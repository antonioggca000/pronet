'use strict';
const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  
  class Contract extends Model {
  
    static associate(models) {
      
      this.belongsTo(models.Client, { foreignKey: 'client_id', as: 'cliente' })
      //this.hasOne(models.Client, { foreignKey: 'client_id', as: 'cliente' })
      this.belongsTo(models.Plan, { foreignKey: 'plan_id', as: 'plan' })


    }
  }

  Contract.init({  
    status: DataTypes.BOOLEAN,
    free: DataTypes.BOOLEAN,
    addition: DataTypes.SMALLINT,   
    expirationday: DataTypes.SMALLINT
  }, {
    sequelize,
    modelName: 'Contract',
    tableName: 'Contracts'
  })

  return Contract;
};