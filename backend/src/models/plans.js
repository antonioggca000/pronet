'use strict';

const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {

  class Plan extends Model {
   
    static associate(models) {
     
      this.belongsTo(models.Server, { foreignKey: 'server_id', as: 'server' })
      this.hasMany(models.Contract, { foreignKey: 'plan_id', as: 'contracts' })

    }
  };

  Plan.init({
    name: DataTypes.STRING,
    value: DataTypes.STRING,
    status: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'Plan',
    tableName: 'Plans'
  });

  return Plan;
};

