'use strict';

const { Model } = require('sequelize')

module.exports = (sequelize, DataTypes) => {

  class Client extends Model {
    static associate(models) {
      
      //this.belongsTo(models.Contract, { foreignKey: 'client_id', as: 'contract' })
      this.hasMany(models.Contract, { foreignKey: 'client_id', as: 'contracts' })
      
      //this.belongsTo(models.Server, { foreignKey: 'server_id', as: 'server' })
      //this.belongsTo(models.Plan, { foreignKey: 'plan_id', as: 'plan' })


    }
  };

  Client.init({
    name: DataTypes.STRING,
    login: DataTypes.STRING,
    password: DataTypes.STRING,
    email: DataTypes.STRING,
    phone: DataTypes.STRING,
    cellphone: DataTypes.STRING,
    tperson: DataTypes.SMALLINT,
    rg: DataTypes.STRING,
    cpf: DataTypes.STRING,
    birthdate: DataTypes.STRING,
    cep: DataTypes.STRING,
    street: DataTypes.STRING,
    number: DataTypes.STRING,
    description: DataTypes.STRING,

    city_select: DataTypes.SMALLINT,
    uf_select: DataTypes.SMALLINT,
    tsubscriber: DataTypes.SMALLINT,

    status: DataTypes.BOOLEAN
  
  }, {
    sequelize,
    modelName: 'Client',
    tableName: 'Clients'
  });

  return Client;
};