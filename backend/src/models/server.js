'use strict';

const { Model } = require('sequelize')

module.exports = (sequelize, DataTypes) => {
  class Server extends Model {
        
    static associate(models) {
    
      this.hasMany(models.Plan, { foreignKey: 'server_id', as: 'plans' })
      //this.hasMany(models.Client, { foreignKey: 'server_id', as: 'clients' })

    }

  };
  Server.init({
    name: DataTypes.STRING,
    address: DataTypes.STRING,
    login: DataTypes.STRING,
    password: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Server',
    tableName: 'Servers'
  });
  return Server;
};