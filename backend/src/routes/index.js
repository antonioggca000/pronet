const routes = require('express').Router()

const ClientRoutes = require('./client')
const ServerRoutes = require('./server')
const PlanRoutes = require('./plan')

routes.use(ServerRoutes)
routes.use(PlanRoutes)
routes.use(ClientRoutes)


module.exports = routes