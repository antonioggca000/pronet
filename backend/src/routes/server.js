'use strict'

const routes = require('express').Router()

const ServerController = require('../controllers/server')

//const ErrorHandler = require('../controllers/ErrorHandler')

routes.post('/integration/server/create', ServerController.create)
routes.put('/integration/server/update/:id', ServerController.update)
routes.delete('/integration/server/delete/:id', ServerController.delete)
routes.get('/integration/server/index', ServerController.index)


//router.use(ErrorHandler.errorHandler)

module.exports = routes