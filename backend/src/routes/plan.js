'use strict'

const routes = require('express').Router()

const PlanController = require('../controllers/plan')

//const ErrorHandler = require('../controllers/ErrorHandler')

routes.post('/integration/plan/create', PlanController.create)
routes.put('/integration/plan/update/:id', PlanController.update)
routes.delete('/integration/plan/delete/:id', PlanController.delete)
routes.get('/integration/plan/:id/sever', PlanController.findByServer)

//router.use(ErrorHandler.errorHandler)

module.exports = routes