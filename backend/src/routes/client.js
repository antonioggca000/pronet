'use strict'

const routes = require('express').Router()

const ClientController = require('../controllers/client')

//const ErrorHandler = require('../controllers/ErrorHandler')

routes.post('/client/store', ClientController.store)
routes.get('/client/index', ClientController.index)
routes.put('/client/update/:id', ClientController.update)

//router.use(ErrorHandler.errorHandler)

module.exports = routes