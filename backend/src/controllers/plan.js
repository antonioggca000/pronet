'use strict'

const { Server, Plan, sequelize } = require('../models')


module.exports = {

    async findByServer(req, res) {

        const id = req.params.id

        if (!(id >= 1 || id <= 999999))
            throw new Error('Código inválido')

        let server = await Server.findByPk(id)

        if (!server)
            throw new Error('Requisição Inválida')


        let plans = await Plan.findAll({
            where: {
                server_id: id
            }
        })

        res.json({
            message: "Plans",
            data: plans,
            status: true
        })

    },

    async index(req, res) {

        let plans = await Plan.findAll()

        res.json({
            message: "Plans",
            data: plans,
            status: true
        })

    },

    async create(req, res) {

        const {
            name,
            value,
            status,
            server_id
        } = req.body

        if (!(server_id >= 1 || server_id <= 999999))
            throw new Error('Código inválido')

        let server = await Server.findByPk(server_id)

        if (!server)
            throw new Error('Servidor inválido')

        let plan = await Plan.create({
            name,
            value,
            status,
            server_id
        })

        // ERROR HANDLER HERE

        res.json({
            message: "Plano Cadastrado",
            data: plan.id,
            status: true
        })


    },

    async update(req, res) {

        const {
            name,
            value,
            status,
            server_id
        } = req.body

        const id = req.params.id

        if (!(id >= 1 || id <= 999999))
            throw new Error('Código inválido')

        let plan = await Plan.findByPk(id)

        if (!plan)
            throw new Error('Requisição Inválida')

        let server = await Server.findByPk(server_id)

        if (!server)
            throw new Error('Servidor inválido')

        await Plan.update({
            name,
            value,
            status,
            server_id
        }, {
            where: {
                id: id
            }
        })


        // ERROR HANDLER HERE


        res.json({
            message: "Dados Atualizados",
            data: id,
            status: true
        })

    },


    async delete(req, res) {

        const id = req.params.id

        if (!(id >= 1 || id <= 999999))
            throw new Error('Código inválido')

        let plan = await Plan.findByPk(id)

        if (!plan)
            throw new Error('Requisição Inválida')

        await Plan.destroy({
            where: {
                id: id
            }
        })


        res.json({
            message: "Plano removido",
            status: true
        })


    }


}