'use strict'

const { Server, sequelize } = require('../models')
const { Op } = require('sequelize')

module.exports = {

    async index(req, res) {

        let servers = await Server.findAll()

        if (!servers)
            throw new Error("Servidores não encontrados")

        return res.json({
            message: "Servers",
            data: servers,
            status: true
        })

    },

    async create(req, res) {

        const {
            name,
            address,
            login,
            password
        } = req.body

        let server = await Server.findOne({
            where: {
                [Op.or]: [
                    { name: name },
                    { address: address }
                ]
            }
        })

        if (server)
            throw new Error('Nome ou endereço existente')



        server = await Server.create({
            name,
            address,
            login,
            passwordt
        })

        if (!server)
            throw new Error("Servidor não cadastrado")


        res.json({
            message: "Servidor Cadastrado",
            data: server.id,
            status: true
        })

    },

    async update(req, res) {

        const {
            name,
            address,
            login,
            password
        } = req.body

        const id = req.params.id

        if (!(id >= 1 || id <= 999999))
            throw new Error('Código inválido')


        let server = await Server.findByPk(id)

        if (!server)
            throw new Error('Requisição Inválida')

        await Server.update({
            name,
            address,
            login,
            password
        }, {
            where: {
                id: id
            }
        })

        res.json({
            message: "Dados Atualizados",
            data: id,
            status: true
        })

    },


    async delete(req, res) {


        const id = req.params.id

        if (!(id >= 1 || id <= 999999))
            throw new Error('Código inválido')

        let server = await Server.findByPk(id)

        if (!server)
            throw new Error('Requisição Inválida')

        await Server.destroy({
            where: {
                id: id
            }
        });

        res.json({
            message: "Servidor removido",
            status: true
        })


    }


}