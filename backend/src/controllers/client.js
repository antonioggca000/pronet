'use strict'

const { Client, Server, Plan, sequelize } = require('../models')
const { Op } = require('sequelize')

module.exports = {

  async index(req, res) {

    let clients = await Client.findAll({
      include: [{
        model: Server,
        as: 'server',
      }]
    })

    /// handler error

    res.json({
      message: "Clientes",
      data: clients,
      status: true
    })

  },

  async store(req, res) {

    const {
      name,
      login,
      password,
      email,
      phone,
      cellphone,
      tperson,
      rg,
      cpf,
      birthdate,
      cep,
      street,
      number,
      description,

      city_select,
      uf_select,
      tsubscriber,

      status

    } = req.body



    let server = await Server.findByPk(server_select)

    if (!server)
      throw new Error('Servidor Inválido')

    let plan = await Plan.findByPk(plan_select)

    if (!plan)
      throw new Error('Plano Inválido')


    let client = await Client.findOne({
      where: {
        login: login
      }
    })

    if (client)
      throw new Error('Nome de Login em uso')


    client = await Client.findOne({
      where: {
        email: email
      }
    })

    if (client)
      throw new Error('Endereço de E-mail em uso')



    client = await Client.create({
      name,
      login,
      password,
      email,
      phone,
      cellphone,
      tperson,
      rg,
      cpf,
      birthdate,
      cep,
      street,
      number,
      description,

      city_select,
      uf_select,
      tsubscriber,

      status

    })


    res.json({
      message: "Cliente Cadastrado",
      data: client.id,
      status: true
    })


  },

  async update(req, res) {

    const {
      name,
      login,
      password,
      email,
      phone,
      cellphone,
      tperson,
      rg,
      cpf,
      birthdate,
      cep,
      street,
      number,
      description,

      city_select,
      uf_select,
      tsubscriber,

      status

    } = req.body


    const id = req.params.id

    if (!(id >= 1 || id <= 999999))
      throw new Error('Código inválido')

    let client = await Client.findByPk(id)

    if (!client)
      throw new Error('Requisição Inválida')

    client = await Client.findOne({
      where: {
        [Op.or]: [
          { login: login },
          { email: email }
        ]
      }
    })

    if (client.id != id)
      throw new Error('Nome de Login ou E-mail em uso')


    let server = await Server.findByPk(server_select)

    if (!server)
      throw new Error('Servidor Inválido')

    let plan = await Plan.findByPk(plan_select)

    if (!plan)
      throw new Error('Plano Inválido')

    await Client.update({
      name,
      login,
      password,
      email,
      phone,
      cellphone,
      tperson,
      rg,
      cpf,
      birthdate,
      cep,
      street,
      number,
      description,

      city_select,
      uf_select,
      tsubscriber,
   
      status
    }, {
      where: {
        id: id
      }
    })


    res.json({
      message: "Dados Atualizados",
      data: id,
      status: true
    })


  },

  // client não pode ser deletado

}

