'use strict'

const router = require('express').Router()

module.exports = {

  async handler(error, req, res, next) {

    console.error(error)

    let code = {
      'status': 500,
      'message': 'Indisponível'
    }

    if (error instanceof ReferenceError)
      code = {
        'status': 500,
        'message': 'Indisponível'
      }
    else
      code = {
        'status': 400,
        'message': error
      }

    return res.status(code.status).json({
      message: code.message,
      status: false
    })

  }

}

