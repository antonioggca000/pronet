'use strict'

const Joi = require('joi')

const { Server, sequelize } = require('../models')


class ServerService {

    constructor(ServerModel = {}){ 
        this._schema = {}
        
        this.schemaInit()
    }

    async create(server){

        //await this._schema.

        await Server.create(server)
            .then((data) => {
                return data
            })
            .catch((err) => {
                throw new Error(err)
            })
        
    }

    async checkServer(name){
        return this._model.findOne({
            where: {
                name: name
            }
        })
    }

    schemaInit() {
        this._schema = Joi.object({
          name: Joi.string()
            .pattern(/[a-zA-Z0-9]/)
            .min(5)
            .max(10)
            .required(),
  
          description: Joi.string()
            .pattern(/[a-zA-Z0-9]/)
            .min(5)
            .max(50)
            .required(),
  
          address: Joi.string().ip({
            version: [
              'ipv4',
              'ipv6'
            ],
            cidr: 'optional'
          }),
  
          login: Joi.string()
            .pattern(/[a-zA-Z0-9]/)
            .min(5)
            .max(50)
            .required(),
  
          password: Joi.string()
            .pattern(/[a-zA-Z0-9!\#$%&'()*+,\-./:;<=>?@\[\\\]^_‘{|}~ ]/)
            .min(5)
            .max(50)
            .required(),
  
  
        })
  
      }


}

//module.exports = ServerRepository










//const errorHandler = require('../exceptions/ErrorHandler')

module.exports = ServerService
