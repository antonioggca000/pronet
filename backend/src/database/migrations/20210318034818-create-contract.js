'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Contracts', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      status: {
        type: Sequelize.BOOLEAN,
        allowNull: false
      },
      free: {
        type: Sequelize.BOOLEAN,
        allowNull: true
      },
      discount: {
        type: Sequelize.SMALLINT,
        allowNull: true
      },
      addition: {
        type: Sequelize.SMALLINT,
        allowNull: true
      },
      expirationday: {
        type: Sequelize.SMALLINT,
        allowNull: true
      },
      client_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        regerences: {
          model: 'Clients',
          key: 'id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'RESTRICT'
      },
      plan_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        regerences: {
          model: 'Plans',
          key: 'id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'RESTRICT'
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Contracts');
  }
};