'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Clients', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      name: {
        type: Sequelize.STRING(100),
        allowNull: false
      },
      login: {
        type: Sequelize.STRING(50),
        allowNull: true,
        unique: true
      },
      password: {
        type: Sequelize.STRING(50),
        allowNull: true
      },
      email: {
        type: Sequelize.STRING(150),
        allowNull: false,
        unique: true
      },
      phone: {
        type: Sequelize.STRING(20),
        allowNull: true
      },
      cellphone: {
        type: Sequelize.STRING(20),
        allowNull: false
      },
      tperson: {
        type: Sequelize.SMALLINT,
        allowNull: false
      },
      rg: {
        type: Sequelize.STRING(20),
        allowNull: true
      },
      cpf: {
        type: Sequelize.STRING(20),
        allowNull: false
      }, 
      birthdate: {
        type: Sequelize.STRING(10),
        allowNull: false
      },
      cep: {
        type: Sequelize.STRING(10),
        allowNull: true
      },
      street: {
        type: Sequelize.STRING(200),
        allowNull: false
      },
      number: {
        type: Sequelize.STRING(10),
        allowNull: false
      },
      description: {
        type: Sequelize.STRING(200),
        allowNull: true
      },
      city_select: {
        type: Sequelize.SMALLINT,
        allowNull: true
      },
      uf_select: {
        type: Sequelize.SMALLINT,
        allowNull: true
      },
      tsubscriber: {
        type: Sequelize.SMALLINT,
        allowNull: true
      },    
      status: {
        type: Sequelize.BOOLEAN,
        allowNull: false
      },          
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Clients');
  }
};